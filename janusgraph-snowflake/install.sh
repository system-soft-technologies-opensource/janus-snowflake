#!/usr/bin/env bash
#
# Copyright 2018 Expero Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#  http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#


# Defaults
JANUS_INSTALL_PATH=""
BUILD_JANUS=true
DIST_JANUS=false
JANUSGRAPH_VERSION=0.3.0

while getopts ":j:b:v:d:" opt; do
  case "${opt}" in
    j)
      JANUS_INSTALL_PATH=$OPTARG
      ;;
    b)
      BUILD_JANUS=$OPTARG
      ;;
    v)
      JANUSGRAPH_VERSION=$OPTARG
      ;;
    d)
      DIST_JANUS=$OPTARG
      ;;
    \?)
      echo "Usage $0 -j [Path t0 JanusGraph] -b [Build JanusGraph] -v [JanusGraph Version] -d [Build JanusGraph Distribution (SLOW)]"
      exit 1
      ;;
  esac
done

# Check if Installer script will use readily available JanusGraph or build it from project
if [ -z $JANUS_INSTALL_PATH ] && [ "$DIST_JANUS" == "false" ];
then
  echo "When JanusGraph Install path is not specified to script (-j), JanusGraph Dist (-d) needs to be true, got false"
  exit 1
fi

# Initialize the global variables w.r.t. project information/directories
JANUS_CORE_CODE=../janusgraph-core
JANUS_TEST_CODE=../janusgraph-test
JANUS_SNOWFLAKE_SRC_CODE=$(pwd)
ROOT_DIR=../

# Default SnowFlake JDBC Version
SNOWFLAKE_VERSION=3.10.3

if [ "$DIST_JANUS" == "true" ]; then
  echo "Building JanusGraph distribution"

  # Infer Branch name/Tag name from Version specified
  JANUS_GIT_BRANCH=v${JANUSGRAPH_VERSION}

  # Checkout the correct version of code
  cd ${ROOT_DIR} && git checkout ${JANUS_GIT_BRANCH}

  # Since we will be building JanusGraph distribution, we will need to build it.
  mvn clean install -Pjanusgraph-release -Dgpg.skip=true -DskipTests=true -Drat.skip=true -Dmaven.javadoc.skip=true -Denforcer.skip=true

  JANUS_DIST_ARCHIVE=janusgraph-dist/target/janusgraph-${JANUSGRAPH_VERSION}.zip

  # Default install path is going to be /opt/janusgraph/...
  mkdir -p /opt/janusgraph/janusgraph-${JANUSGRAPH_VERSION}

  # Unzip the distribution to required directory created
  JANUS_INSTALL_PATH=/opt/janusgraph/janusgraph-${JANUSGRAPH_VERSION}

  # Unzip
  unzip ${JANUS_DIST_ARCHIVE} -d ${JANUS_INSTALL_PATH}

  echo "JanusGraph successfully build from source dode and installed"

  # Revert back to original branch 'snowflake' so that now plugin can be build
  cd ${ROOT_DIR} && git checkout snowflake

  echo "Reverted back to snowflake branch after building JanusGraph dist"
fi

if [[ ! -d "$JANUS_INSTALL_PATH" ]]; then
  echo "Directory ${JANUS_INSTALL_PATH} does not exist"
  exit 1
fi

# Installed JanusGraph path constants
JANUS_BIN_PATH=${JANUS_INSTALL_PATH}/bin
JANUS_CONF_PATH=${JANUS_INSTALL_PATH}/conf
JANUS_GREMLIN_SERVER_CONF=${JANUS_CONF_PATH}/gremlin-server

# Generate Maven home path for SnowFlake JAR
MAVEN_SNOWFLAKE_HOME=/root/.m2/repository/net/snowflake/snowflake-jdbc/${SNOWFLAKE_VERSION}/

# Build JanusGraph code if build parameter is provided as true
if [ "${BUILD_JANUS}" == "true" ]; then
    echo "Building JanusGraph core"

    JANUS_GIT_BRANCH=v${JANUSGRAPH_VERSION}
    cd ${JANUS_CORE_CODE} && git checkout ${JANUS_GIT_BRANCH}
    cd ${JANUS_CORE_CODE} && mvn clean compile package -Drat.skip=true -Dmaven.javadoc.skip=true -Denforcer.skip=true

    echo "JanusGraph Core build done"

    echo "Building JanusGraph test"

    JANUS_GIT_BRANCH=v${JANUSGRAPH_VERSION}
    cd ${JANUS_TEST_CODE} && git checkout ${JANUS_GIT_BRANCH}
    cd ${JANUS_TEST_CODE} && mvn clean compile package -Drat.skip=true -Dmaven.javadoc.skip=true -Denforcer.skip=true

    echo "JanusGraph test build done"

    # Revert back to original branch 'snowflake' so that now plugin can be build
    cd ${ROOT_DIR} && git checkout snowflake

    echo "Reverted back to snowflake branch after building JanusGraph core & test"
fi

# Update POM with the required/passed version numbers.
# Update JanusGraph version with the one passed using sed
JANUS_STRING_TO_REPLACE="<janusgraph.version>\${project.version}</janusgraph.version>"
JANUS_REPLACED_STRING="<janusgraph.version>${JANUSGRAPH_VERSION}</janusgraph.version>"
sed -i "s|${JANUS_STRING_TO_REPLACE}|${JANUS_REPLACED_STRING}|g" ${JANUS_SNOWFLAKE_SRC_CODE}/pom.xml
# Update SnowFlake version with the assigned version number
SF_STRING_TO_REPLACE="<snowflake.jdbc.version>3.10.3</snowflake.jdbc.version>"
SF_REPLACED_STRING="<snowflake.jdbc.version>${SNOWFLAKE_VERSION}</snowflake.jdbc.version>"
sed -i "s|${SF_STRING_TO_REPLACE}|${SF_REPLACED_STRING}|g" ${JANUS_SNOWFLAKE_SRC_CODE}/pom.xml
# The above mutations needs to be reverted back so that we don't leave behind any permanent undesired changes.

# Build the required projects
echo "Building JanusGraph SnowFlake Storage Adopter plugin"
cd ${ROOT_DIR}
# Build JanusGraph-SnowFlake plugin
cd ${JANUS_SNOWFLAKE_SRC_CODE} && mvn -Drat.skip=true -Denforcer.skip=true clean compile package test install
# -DskipTests
cd ${ROOT_DIR}
echo "JanusGraph SnowFlake plugin build done"

# Copy the dependency jars
# Copy plugin jars
cp -rf ${JANUS_SNOWFLAKE_SRC_CODE}/target/*.jar ${JANUS_INSTALL_PATH}/ext
echo "Copied JanusGraph SnowFlake Plugin jars"
# Copy snowflake jars
cp -rf ${MAVEN_SNOWFLAKE_HOME}/*.jar ${JANUS_INSTALL_PATH}/ext
echo "Copied SnowFlake JDBC jars"
# Copy JanusGraph core jar
if [ "${BUILD_JANUS}" == "true" ]; then
    cd ${JANUS_SNOWFLAKE_SRC_CODE}
    cp ${JANUS_CORE_CODE}/target/janusgraph-core-${JANUSGRAPH_VERSION}.jar ${JANUS_INSTALL_PATH}/lib
    cp ${JANUS_TEST_CODE}/target/janusgraph-test-${JANUSGRAPH_VERSION}.jar ${JANUS_INSTALL_PATH}/lib
    echo "Copied JanusGraph Core jars"
fi

# Copy the configuration files
cp -rf ${JANUS_SNOWFLAKE_SRC_CODE}/conf/janusgraph-snowflake.properties ${JANUS_CONF_PATH}
cp -rf ${JANUS_SNOWFLAKE_SRC_CODE}/conf/janusgraph-snowflake-es-server.properties ${JANUS_GREMLIN_SERVER_CONF}

# backup Gremlin server config
mv ${JANUS_GREMLIN_SERVER_CONF}/gremlin-server.yaml ${JANUS_GREMLIN_SERVER_CONF}/gremlin-server.yaml.orig
cp -rf ${JANUS_SNOWFLAKE_SRC_CODE}/conf/gremlin-server.yaml ${JANUS_GREMLIN_SERVER_CONF}/gremlin-server.yaml
# backup janusgraph.sh
mv ${JANUS_BIN_PATH}/janusgraph.sh ${JANUS_BIN_PATH}/janusgraph.sh.orig
cp -rf ${JANUS_SNOWFLAKE_SRC_CODE}/conf/janusgraph.sh ${JANUS_BIN_PATH}/janusgraph.sh

# Revert back the changes in pom.xml
# Revert back SnowFlake changes
sed -i "s|${SF_REPLACED_STRING}|${SF_STRING_TO_REPLACE}|g" ${JANUS_SNOWFLAKE_SRC_CODE}/pom.xml
# Revert back JanusGraph changes
sed -i "s|${JANUS_REPLACED_STRING}|${JANUS_STRING_TO_REPLACE}|g" ${JANUS_SNOWFLAKE_SRC_CODE}/pom.xml
echo "installation done......"
