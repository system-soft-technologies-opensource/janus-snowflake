package com.sstech.janusgraph.snowflake.utils;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class OperationStatus {
    public boolean SUCCESS = true;
    public boolean FAILED = false;

    public boolean KEYEXIST = true;

    private boolean operationStatus;
    private Object resultSet;

    public OperationStatus(boolean status) {
        operationStatus = status;
    }

    public void setKEYEXIST(boolean KEYEXIST) {
        this.KEYEXIST = KEYEXIST;
    }

    public void setResult(Object result) {
        resultSet = result;
    }

    public Object getResult() {
        return resultSet;
    }

    public ArrayList<String> getResultAsList(String columnName) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ResultSet resultSet = (ResultSet) this.resultSet;
            ArrayList<String> data = new ArrayList<>();
            while (resultSet.next()) {
                data.add(resultSet.getString(columnName));
            }
            return data;
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<ArrayList> getResultAsList(String... columns) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ResultSet resultSet = (ResultSet) this.resultSet;
            ArrayList<ArrayList> data = new ArrayList<>();
            while (resultSet.next()) {
                ArrayList<String> rowData = new ArrayList<>();
                for (String column : columns) {
                    rowData.add(resultSet.getString(column));
                }
                data.add(rowData);
            }
            return data;
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<String> getResultAsList(Integer columnIndex) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ResultSet resultSet = (ResultSet) this.resultSet;
            ArrayList<String> data = new ArrayList<>();
            while (resultSet.next()) {
                data.add(resultSet.getString(columnIndex));
            }
            return data;
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public ArrayList<ArrayList> getResultAsList(Integer... columns) throws SQLException, UnsupportedOperationException {
        if (resultSet instanceof ResultSet) {
            ResultSet resultSet = (ResultSet) this.resultSet;
            ArrayList<ArrayList> data = new ArrayList<>();
            while (resultSet.next()) {
                ArrayList<String> rowData = new ArrayList<>();
                for (Integer columnIdx : columns) {
                    rowData.add(resultSet.getString(columnIdx));
                }
                data.add(rowData);
            }
            return data;
        } else
            throw new UnsupportedOperationException("getResultAsList() method can only be called when the " +
                "return type of query is expected ResultSet. Found " + resultSet.getClass());
    }

    public void setOperationStatus(boolean status) {
        operationStatus = status;
    }

    public boolean getStatus() {
        return operationStatus;
    }

}
