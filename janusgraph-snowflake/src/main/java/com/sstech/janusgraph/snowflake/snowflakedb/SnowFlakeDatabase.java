// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.common.utils.SingleFileLogger;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

public class SnowFlakeDatabase {
    private String tableName;
    private SnowFlakeTransaction transaction;
    private SnowFlakeConnection connection;
    private static final Logger log = LoggerFactory.getLogger(SnowFlakeDatabase.class);

    public SnowFlakeDatabase(String name, SnowFlakeTransaction tx) throws IOException, SnowFlakeConnectionException {
        transaction = tx;
        tableName = name;
        connection = transaction.getConnection();
        if (!tableName.equals("")) {
            getOrCreateTable(tableName);
        }
    }

    public SnowFlakeDatabase(String name, SnowFlakeConnection connection) throws IOException {
        // TODO : Need to add GET OR CREATE TABLE SnowFlake statement & execute the query
        tableName = name;
        this.connection = connection;
    }

    private Boolean getOrCreateTable(String tableName) {
        String sql = "CREATE TABLE " + tableName + " IF NOT EXISTS (KEY BINARY, VALUE BINARY, COLUMN1 BINARY)";
        log.info(String.format("Creating table %s with [%s]", tableName, sql));
        OperationStatus res = connection.executeUpdate(sql);
        return res.getStatus();
    }

    private byte[] getByteArray(String encodedString) {
        return Base64.getDecoder().decode(encodedString);
    }

    private StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

    public SnowFlakeConnection getConnection() {
        return connection;
    }

    public OperationStatus put(StaticBuffer key, StaticBuffer value) {
        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error

        String statement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
            "put", tableName, key.toString(), value.toString(), statement));

        return connection.executeUpdate(statement);
    }

    public OperationStatus put(byte[] key, byte[] value) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        String valueBase64Encoded = Base64.getEncoder().encodeToString(value);

        // This currently is generic put, without checking if entry exists in DB. Need to implement
        // putNoOverWrite which shall insert only when data is absent else throw error
        String insertStatement = "INSERT INTO " + tableName
            + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
            "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
            "put(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

        return connection.executeUpdate(insertStatement);
    }

    public OperationStatus putNotOverWrite(StaticBuffer key, StaticBuffer value) {
        String statement = "SELECT COUNT(*) FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64')";
        if ((Integer) connection.executeQuery(statement).getResult() > 0) {
            String insertStatement = "INSERT INTO " + tableName + " (KEY, VALUE) VALUES " + key + value;
            return connection.executeUpdate(insertStatement);
        } else {
            OperationStatus status = new OperationStatus(false);
            status.setKEYEXIST(true);
            return status;
        }
    }

    public OperationStatus putNotOverWrite(byte[] key, byte[] value) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        String valueBase64Encoded = Base64.getEncoder().encodeToString(value);
        Integer count = 0;
        String insertStatement = "INSERT INTO " + tableName
            + " (KEY, VALUE) VALUES (to_binary('" + keyBase64Encoded + "', 'BASE64'), " +
            "to_binary('" + valueBase64Encoded + "', 'BASE64'))";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, value=%s, sql=%s",
            "putNotOverWrite(encoded)", tableName, keyBase64Encoded, valueBase64Encoded, insertStatement));

        return connection.executeUpdate(insertStatement);
    }

    public OperationStatus delete(StaticBuffer key) {

        String statement = "TRUNCATE * FROM " + tableName + " WHERE KEY = " + key;

        return connection.executeUpdate(statement);
    }

    public OperationStatus delete(byte[] key) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

        String statement = "DELETE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
            "delete(encoded)", tableName, keyBase64Encoded, statement));


        return connection.executeUpdate(statement);
    }

    public OperationStatus get(String key) throws SQLException {
        String qry = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64') limit 1";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
            "get(encoded)", tableName, key, qry));

        return connection.executeQuery(qry);
    }

    public KeyValueEntry get(String key, String value) throws SQLException {
        String qry = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + key + "', 'BASE64') AND VALUE = to_binary('" + value + "', 'BASE64') limit 1";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
            "get(encoded)", tableName, key, qry));

        OperationStatus res = connection.executeQuery(qry);
        ResultSet resultSet = (ResultSet) res.getResult();

        KeyValueEntry entry = null;

        while (resultSet.next()) {
            String keyEncoded = resultSet.getString("KEY");
            String valueEncoded = resultSet.getString("VALUE");

            byte[] byteKey = getByteArray(keyEncoded);
            byte[] byteValue = getByteArray(valueEncoded);

            StaticBuffer keySB = getBuffer(byteKey);
            StaticBuffer valueSB = getBuffer(byteValue);

            entry = new KeyValueEntry(keySB, valueSB);
        }

        return entry;
    }

    public OperationStatus get(byte[] key) {
        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);

        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";

        log.info(LogUtils.stringFormatter("SnowFlakeDB: op=%s, tbl=%s, key=%s, sql=%s",
            "get(encoded)", tableName, keyBase64Encoded, statement));

        return connection.executeQuery(statement);
    }

    public OperationStatus getAll() {
        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName;
        return connection.executeQuery(statement);
    }

    public OperationStatus getAll(Integer limit) {
        String statement = "SELECT TO_VARCHAR(KEY,'BASE64') as KEY, TO_VARCHAR(VALUE,'BASE64') as VALUE FROM " + tableName + " LIMIT " + limit;
        return connection.executeQuery(statement);
    }

    public void close() throws PermanentBackendException {
        OperationStatus status = connection.close();
        log.info("Successfully closed connection from SnowFlake DB class");
        if (!status.getStatus()) {
            throw new PermanentBackendException("Could not close connection to Graph: " + status);
        }
    }

    public SnowFlakeCursor openCursor() throws IOException {
        SnowFlakeCursor cursor = new SnowFlakeCursor(getConnection());
        cursor.setTable(tableName);
        log.info("Successfully created cursor connection to SnowFlake using custom SnowFlakeCursor class");
        return cursor;
    }

    private void initializeDatabase() throws Exception {

        SnowFlakeDatabase sdb = new SnowFlakeDatabase(tableName, transaction);
        sdb.close();
    }

    public OperationStatus getTables(String dbName, String schema) {
        try {
            String listOfTableQry = "show tables in " + dbName + "." + schema;

            OperationStatus result = connection.executeQuery(listOfTableQry);
            ResultSet resultSet = ((ResultSet) result.getResult());

            List<String> tableList = new ArrayList<String>();

            while (resultSet.next()) {
                tableList.add(resultSet.getString("name"));

            }
            OperationStatus status = new OperationStatus(true);
            status.setResult(tableList);
            log.info("Successfully fetched list of tables as " + tableList.toString());

            return status;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    //remove tables
    public OperationStatus removeTable(String schema, String tableName) {
        try {
            String dropTable = "drop table if exists " + schema + "." + tableName;
            OperationStatus result = connection.executeQuery(dropTable);
            ResultSet resultSet = (ResultSet) result.getResult();
            while (resultSet.next()) {
                return new OperationStatus(true);
            }
            log.info("Successfully removed table " + tableName);
        } catch (Exception e) {
            e.printStackTrace();
            log.error(LogUtils.logTraceback("Unable to remove table " + tableName, e));
        }
        return new OperationStatus(true);
    }

    public String getTableName() {
        return tableName;
    }
}
