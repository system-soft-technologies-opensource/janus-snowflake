package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.utils.LogUtils;
import net.snowflake.client.jdbc.SnowflakeSQLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class SnowFlakeCreateStatement {

    private Connection connection;
    private Statement statement;
    private static final Logger log = LoggerFactory.getLogger(SnowFlakeCreateStatement.class);

    public SnowFlakeCreateStatement(Connection snowFlakeConnection) throws SQLException, IOException {
        connection = snowFlakeConnection;
        statement = connection.createStatement();
    }

    public int executeUpdate(String sql) throws SQLException {
        log.info(LogUtils.stringFormatter("Successfully executed update %s from cursor", sql));
        return statement.executeUpdate(sql);
    }

    public ResultSet executeQuery(String sql) throws SQLException {
        log.info("Executing SQL query " + sql + " from cursor");
        try {
            return connection.createStatement().executeQuery(sql);
        } catch (SnowflakeSQLException e) {
            e.printStackTrace();
            System.exit(-1);
            throw new SQLException("Unable to run the query " + sql + " Is statement null? " + String.valueOf(statement == null) + String.valueOf(connection == null));
        }
    }

    public void close() throws SQLException {
        try {
            statement.close();
            log.info("Successfully closed connection for statement object in cursor");
            connection.close();
            log.info("Successfully closed connection for JDBC connection object in cursor");
        } catch (SQLException e) {
            log.error("Unable to close connection to SnowFlake from cursor");
        }
    }
}
