package com.sstech.janusgraph.snowflake.connection;

import com.jolbox.bonecp.BoneCP;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeCreateStatement;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Properties;

public class SnowFlakeConnection {
    private static final Logger log = LoggerFactory.getLogger(SnowFlakeConnection.class);
    private static SnowFlakeConnection instance = null;
    private String URL = "";
    private Integer port = 0;
    private String warehouse = "";
    private String extra = "";
    private String userName = "";
    private String pwd = "";
    private String schema = "";
    private String db = "";
    private String role = "";

    private String connectionURL = "";
    private Connection snowflakeJdbcConnection;
    private BoneCP connectionPool;
    private SnowFlakeCreateStatement createStatement;

    private boolean autoCommit = false;

    private SnowFlakeConnection(boolean auto_commit) throws IOException {
        autoCommit = auto_commit;
    }

    public static SnowFlakeConnection getInstance(boolean auto_commit) throws IOException {
        if (instance == null) {
            instance = new SnowFlakeConnection(auto_commit);
        }
        return instance;
    }

    public String getURL() {
        return URL;
    }

    public void setURL(String path) {
        URL = path;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setPort(Integer portNumber) {
        port = portNumber;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setWarehouse(String warehouse_name) {
        warehouse = warehouse_name;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setOthers(String others) {
        extra = others;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setUserName(String userName) {
        this.userName = userName;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setPassword(String pwd) {
        this.pwd = pwd;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setDB(String DB) {
        this.db = DB;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setSchema(String schema) {
        this.schema = schema;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    public void setRole(String role) {
        this.role = role;

        if (areParametersSet()) {
            buildConnectionURL();
        }
    }

    private boolean areParametersSet() {
        return !URL.equals("") && port != 0 && !warehouse.equals("");
    }

    private void connect() throws SQLException {
        // build connection properties
        Properties properties = new Properties();
        properties.put("user", this.userName);        // replace "" with your user name
        properties.put("password", this.pwd);    // replace "" with your password
        properties.put("warehouse", this.warehouse);   // replace "" with target warehouse name
        properties.put("db", this.db);          // replace "" with target database name
        properties.put("schema", this.schema);      // replace "" with target schema name
        properties.put("role", this.role);
        String connectUrl = this.URL;

        String driverClass = "net.snowflake.client.jdbc.SnowflakeDriver";
        try {
            log.debug("Load driver class: " + driverClass);
            Class.forName(driverClass);
        } catch (ClassNotFoundException ex) {
            log.debug("NOT FOUND: " + driverClass);
            throw new RuntimeException(ex);
        }

        log.debug("properties: " + properties);
        snowflakeJdbcConnection = DriverManager.getConnection(connectUrl, properties);
    }

    private void buildConnectionURL() {
        connectionURL = URL;
    }

    private void buildConnection() throws SQLException, IOException {
        this.connect();
        createStatement = new SnowFlakeCreateStatement(snowflakeJdbcConnection);
        log.info("Successfully created 'createStatement' for JDBC connection to execute queries");
    }

    public SnowFlakeConnection getConnection() throws SQLException, IOException {
        this.buildConnection();
        return this;
    }

    public OperationStatus executeUpdate(String sql) {
        log.info("Executing SQL update " + sql);
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeUpdate(sql));
            log.info(LogUtils.stringFormatter("Successfully executed update %s", sql));
            return status;
        } catch (SQLException e) {
            log.error("Unable to run update " + sql);
            return new OperationStatus(false);
        }
    }

    public OperationStatus executeQuery(String sql) {
        log.info("Executing SQL query " + sql);
        try {
            OperationStatus status = new OperationStatus(true);
            status.setResult(createStatement.executeQuery(sql));
            log.info(LogUtils.stringFormatter("Successfully executed query %s", sql));
            return status;
        } catch (SQLException e) {
            log.error("Unable to run query " + sql);
            return new OperationStatus(false);
        }
    }

    public ResultSet executeCursorQuery(String sql) {
        log.info("Executing cursor query " + sql);
        try {
            ResultSet resultSet = createStatement.executeQuery(sql);
            log.info(LogUtils.stringFormatter("Number of columns fetched executing %s SQL is %s", sql, String.valueOf(resultSet.getMetaData().getColumnCount())));
            return resultSet;
        } catch (SQLException e) {
            e.printStackTrace();
            return null;
        }
    }

    public OperationStatus close() {
        log.info("Closing connection to SnowFlake");
        try {
            createStatement.close();
            log.info("Closed connection from createStatement");
            snowflakeJdbcConnection.close();
            log.info("Closed JDBC connection");
            return new OperationStatus(true);
        } catch (SQLException e) {
            log.error("Couldn't close connection to SnowFlakeDB");
            return new OperationStatus(false);
        }
    }
}
