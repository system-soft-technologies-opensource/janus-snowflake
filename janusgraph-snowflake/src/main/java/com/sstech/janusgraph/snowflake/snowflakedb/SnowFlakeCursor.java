package com.sstech.janusgraph.snowflake.snowflakedb;

import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.sql.ResultSet;
import java.util.Base64;

public class SnowFlakeCursor {
    private SnowFlakeConnection connection;
    private SnowFlakeTransaction txh;
    private String tableName;

    private static final Logger log = LoggerFactory.getLogger(SnowFlakeCursor.class);

    public SnowFlakeCursor(SnowFlakeConnection snowFlakeConnection) {
        connection = snowFlakeConnection;
    }

    @Override
    public String toString() {
        return tableName;
    }

    public void setTable(String table) {
        tableName = table;
    }

    public ResultSet getSearchKey(StaticBuffer key) {
        String statement = "SELECT * FROM " + tableName + " WHERE KEY = " + key.toString();

        ResultSet resultSet = connection.executeCursorQuery(statement);
        return resultSet;
    }

    public ResultSet getSearchKey(byte[] key) throws BackendException {
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, key=%s", "getSearchKey", tableName, key.toString()));

        String keyBase64Encoded = Base64.getEncoder().encodeToString(key);
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, encodedKey=%s", "getSearchKey", tableName, keyBase64Encoded));

        String statement = "SELECT * FROM " + tableName + " WHERE KEY = to_binary('" + keyBase64Encoded + "', 'BASE64')";
        log.info(LogUtils.stringFormatter("op=%s, tbl=%s, sql=%s", "getSearchKey", tableName, statement));
        ResultSet resultSet = connection.executeCursorQuery(statement);
        return resultSet;
    }
}
