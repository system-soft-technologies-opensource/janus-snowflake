package com.sstech.janusgraph.examples;

import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.common.AbstractStoreTransaction;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

public class SnowFlakeTransaction extends AbstractStoreTransaction {

    private final SnowFlakeStoreManager storeManager;

    public SnowFlakeTransaction(final BaseTransactionConfig config, SnowFlakeStoreManager manager) {
        super(config);
        this.storeManager = manager;
    }

    public void execute(String statement) {

    }

    private static Connection getConnection()
        throws SQLException {
        try {
            Class.forName("com.snowflake.client.jdbc.SnowflakeDriver");
        } catch (ClassNotFoundException ex) {
            System.err.println("Driver not found");
        }

        Properties properties = new Properties();
        properties.put("user", "Manjunath");     // replace "" with your username
        properties.put("password", ""); // replace "" with your password
        properties.put("warehouse", "COMPUTE_WH");
        properties.put("db", "GRAPHDBTEST");       // replace "" with target database name
        properties.put("schema", "GRAPHDBTEST");   // replace "" with target schema name
        properties.put("role", "SYSADMIN");
        String connectStr = System.getenv("SF_JDBC_CONNECT_STRING");
        return DriverManager.getConnection(connectStr, properties);
    }

}
