package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.diskstorage.snowflake.keycolumnvalue.SnowFlakeStoreTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnectionManager;
import org.janusgraph.diskstorage.*;
import org.janusgraph.diskstorage.keycolumnvalue.*;
import org.janusgraph.diskstorage.util.StandardBaseTransactionConfig;
import org.janusgraph.diskstorage.util.time.TimestampProviders;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Map;
import java.util.Properties;

public class SnowFlakeKCVStoreManager implements KeyColumnValueStoreManager {
    private static final Logger log = LoggerFactory.getLogger(SnowFlakeKCVStoreManager.class);

    private SnowFlakeConnectionManager connectionManager() throws IOException, PermanentBackendException {
        String clusterFilePath = "sf-cluster-credentials.properties";
        File configFile = new File(clusterFilePath);
        log.debug("Reading configuration from" + configFile.getAbsolutePath());
        log.debug("" + configFile.exists());
        InputStream inputHandler = new FileInputStream(clusterFilePath);
        Properties config = new Properties();
        config.load(inputHandler);
        String url = config.getProperty("URL");
        String userName = config.getProperty("userName");
        String password = config.getProperty("password");
        String warehouse = config.getProperty("warehouse");
        String db = config.getProperty("DB");
        String schema = config.getProperty("schema");
        String role = config.getProperty("role");
        return SnowFlakeConnectionManager.getInstance(config);
    }

    @Override
    public KeyColumnValueStore openDatabase(String name, StoreMetaData.Container metaData) throws BackendException {
        try {
            SnowFlakeConnectionManager sfcm = connectionManager();
            sfcm.connect();
            return new SnowFlakeKVCStore(sfcm.getConnection());
        } catch (IOException e) {
            throw new PermanentBackendException(e);
        } catch (SnowFlakeConnectionException e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public void mutateMany(Map<String, Map<StaticBuffer, KCVMutation>> mutations, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();

    }

    @Override
    public StoreTransaction beginTransaction(BaseTransactionConfig config) throws BackendException {
        return new SnowFlakeStoreTransaction(StandardBaseTransactionConfig.of(TimestampProviders.NANO));
    }

    @Override
    public void close() throws BackendException {
        // TODO: Implement Close operation
    }

    @Override
    public void clearStorage() throws BackendException {
        log.debug("Entering clearStorage");
        // TODO: Delete all storage
        log.debug("Exiting clearStorage returning:void");

    }

    @Override
    public boolean exists() throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public StoreFeatures getFeatures() {
        return null;
    }

    @Override
    public String getName() {
        log.debug("Entering getName");
        final String name = getClass().getSimpleName();
        log.debug("Exiting getName returning:{}", name);
        return name;
    }

    @Override
    public List<KeyRange> getLocalKeyPartition() throws BackendException {
        throw new UnsupportedOperationException();
    }
}
