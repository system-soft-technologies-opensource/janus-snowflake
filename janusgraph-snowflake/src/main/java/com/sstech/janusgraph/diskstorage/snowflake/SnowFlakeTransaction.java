// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.


package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnectionManager;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.common.AbstractStoreTransaction;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SnowFlakeTransaction extends AbstractStoreTransaction {
    private static final Logger log = LoggerFactory.getLogger(SnowFlakeTransaction.class);
    private final SnowFlakeStoreManager storeManager;
    private SnowFlakeConnectionManager connectionManager = SnowFlakeConnectionManager.getInstance(3000);
    private SnowFlakeConnection connection;

    private String URL = "";
    private String userName = "";
    private String pwd = "";
    private String warehouse = "";
    private String DB = "";
    private String schema = "";
    private String role = "";

    public SnowFlakeTransaction(final BaseTransactionConfig config, SnowFlakeStoreManager manager) throws PermanentBackendException {
        super(config);
        this.storeManager = manager;
        setConnectionParameters();
    }

    public SnowFlakeTransaction(final BaseTransactionConfig config, SnowFlakeStoreManager manager,
                                SnowFlakeConnection connection) throws PermanentBackendException {
        super(config);
        this.storeManager = manager;
        this.connection = connection;
        setConnectionParameters();
    }

    private void setConnectionParameters() {
        URL = System.getenv("URL");
        userName = System.getenv("userName");
        warehouse = System.getenv("warehouse");
        pwd = System.getenv("password");
        DB = System.getenv("DB");
        schema = System.getenv("schema");
        role = System.getenv("role");
        log.info(LogUtils.stringFormatter("Set SnowFlake connection parameters as URL=%s, userName=%s, " +
            "warehouse=%s, password=%s, DB=%s, schema=%s, role=%s", URL, userName, warehouse, pwd, DB, schema, role));
    }

    private void connectToSnowFlake() throws SnowFlakeConnectionException {
        connectionManager.setURL(URL);
        connectionManager.setUserName(userName);
        connectionManager.setPassword(pwd);
        connectionManager.setWarehouse(warehouse);
        connectionManager.setDB(DB);
        connectionManager.setSchema(schema);
        connectionManager.setRole(role);

        connectionManager.connect();

        log.info("Connected to SnowFlakeConnectionManager class with specified configurations");
    }

    public SnowFlakeConnection getConnection() throws SnowFlakeConnectionException {
        log.info("Successfully retrieved connection from SnowFlakeConnectionManager.");
        return this.connection;
    }

    public void close() {
        connectionManager.close();
    }

    @Override
    public String toString() {
        return "URL=" + URL;
    }
}
