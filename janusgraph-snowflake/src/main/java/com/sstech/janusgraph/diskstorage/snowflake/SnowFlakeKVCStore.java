package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.Entry;
import org.janusgraph.diskstorage.EntryList;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.*;
import org.janusgraph.diskstorage.util.EntryArrayList;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
import org.janusgraph.diskstorage.util.StaticArrayEntry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class SnowFlakeKVCStore implements KeyColumnValueStore {
    Logger log = LoggerFactory.getLogger(SnowFlakeKVCStore.class);
    private SnowFlakeConnection sfconn;

    public SnowFlakeKVCStore(SnowFlakeConnection conn) {
        this.sfconn = conn;
    }

    @Override
    public EntryList getSlice(KeySliceQuery query, StoreTransaction txh) throws BackendException {
        // select * from KCV_STORE where COLVALS:col0::String between 'val0' and 'val1';
        StaticBuffer keyBuff = query.getKey();
        StaticBuffer sliceStart = query.getSliceStart();
        StaticBuffer sliceEnd = query.getSliceEnd();

        String keyStr = DatatypeConverter.printHexBinary(keyBuff.asByteBuffer().array());
        String start = new String(sliceStart.asByteBuffer().array(), StandardCharsets.UTF_8);
        String end = new String(sliceEnd.asByteBuffer().array(), StandardCharsets.UTF_8);
        // SELECT DISTINCT KS.KEY, COLVALS FROM KCV_STORE ks, LATERAL FLATTEN(ks.COLVALS) l WHERE ks.KEY = TO_BINARY('0000000000000000') AND l.KEY BETWEEN 'col0' AND 'col1';
        String sql = String.format("SELECT DISTINCT ks.KEY, l.PATH, l.VALUE::String FROM KCV_STORE ks, LATERAL FLATTEN(ks.COLVALS) l WHERE ks.KEY = TO_BINARY('%s') AND l.KEY BETWEEN '%s' AND '%s'",
            keyStr, start, end
        );
        ResultSet resultSet = sfconn.executeCursorQuery(sql);

        List<Entry> list = new ArrayList<>();
        try {
            while (resultSet.next()) {
                byte[] bytes = resultSet.getBytes(1);
                byte[] columnBytes = resultSet.getBytes(2);
                byte[] valueBytes = resultSet.getBytes(3);
                StaticBuffer colBuf = StaticArrayBuffer.of(columnBytes);
                StaticBuffer valBuf = StaticArrayBuffer.of(valueBytes);
                log.debug(String.format("%s => %s -> %s", DatatypeConverter.printHexBinary(bytes),
                    new String(columnBytes, StandardCharsets.UTF_8), new String(valueBytes, StandardCharsets.UTF_8)));
                Entry e = StaticArrayEntry.of(colBuf, valBuf);
                list.add(e);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return EntryArrayList.of(list);
    }

    @Override
    public Map<StaticBuffer, EntryList> getSlice(List<StaticBuffer> keys, SliceQuery query, StoreTransaction txh) throws BackendException {
        Map<StaticBuffer, EntryList> map = new HashMap<>();
        StaticBuffer sliceStart = query.getSliceStart();
        StaticBuffer sliceEnd = query.getSliceEnd();
        for (StaticBuffer key : keys) {
            KeySliceQuery ksq = new KeySliceQuery(key, sliceStart, sliceEnd);
            EntryList entryList = getSlice(ksq, txh);
            map.put(key, entryList);
        }
        return map;
    }

    @Override
    public void mutate(StaticBuffer key, List<Entry> additions, List<StaticBuffer> deletions, StoreTransaction txh) throws BackendException {
        Map<String, Object> additionMap = new TreeMap<>();

        ByteBuffer keyBuf = key.asByteBuffer();
        byte[] keyArr = getByteArray(keyBuf);
        String keyStr = new String(keyArr, StandardCharsets.UTF_8);
        log.debug("keyStr: " + keyStr);
        log.debug("Additions");
        for (Entry e : additions) {
            StaticBuffer column = e.getColumn();
            StaticBuffer value = e.getValue();
            byte[] colBytes = getByteArray(column.asByteBuffer());
            String k = new String(colBytes, StandardCharsets.UTF_8);
            byte[] valBytes = getByteArray(value.asByteBuffer());
            String v = new String(valBytes, StandardCharsets.UTF_8);
            log.debug(String.format("%s ==> %s", k, v));
            additionMap.put(k, v);
        }

        log.debug("Deletions");
        Map<String, Object> deletionMap = new TreeMap<>();
        for (StaticBuffer column : deletions) {
            byte[] byteArray = getByteArray(column.asByteBuffer());
            String k = new String(byteArray, StandardCharsets.UTF_8);
            log.debug(k);
            deletionMap.put(k, null);
        }

        log.debug(String.format("Final  %s ==> %s", key, additionMap));
        log.debug("DEBUG");

        String hexKey = DatatypeConverter.printHexBinary(keyArr);
        String countQuery = String.format(
            "SELECT count(*) FROM KCV_STORE WHERE KEY = to_binary('%s');", hexKey);
        log.debug(countQuery);
        boolean keyExists = false;
        ResultSet rs = sfconn.executeCursorQuery(countQuery);
        try {
            rs.next();
            int count = rs.getInt(1);
            if (count > 0) {
                keyExists = true;
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }

        if (keyExists) {
            // update
            String objectMembers = "k.COLVALS";
            for (String k : additionMap.keySet()) {
                objectMembers = String.format("OBJECT_INSERT(%s, '%s', '%s', TRUE)", objectMembers, k, additionMap.get(k));
            }

            for (String k : deletionMap.keySet()) {
                objectMembers = String.format("OBJECT_INSERT(%s, '%s', NULL, TRUE)", objectMembers, k);
            }

            String updateSql = String.format(
                "UPDATE KCV_STORE k SET k.COLVALS = %s WHERE k.KEY = TO_BINARY('%s');", objectMembers, hexKey);
            log.debug(updateSql);
            OperationStatus operationStatus = sfconn.executeQuery(updateSql);
            log.debug(operationStatus + "");
        } else {
            // insert
            String objectMembers = "";
            boolean first = true;
            for (String k : additionMap.keySet()) {
                String pair = String.format("'%s', '%s'", k, additionMap.get(k));
                if (first) {
                    first = false;
                } else {
                    objectMembers += ", ";
                }
                objectMembers += pair;
            }
            String insertSql = String.format("INSERT INTO KCV_STORE SELECT to_binary('%s'), OBJECT_CONSTRUCT(%s);",
                hexKey, objectMembers);
            log.debug(insertSql);
            OperationStatus operationStatus = sfconn.executeQuery(insertSql);
            log.debug(operationStatus + "");
        }
    }

    private byte[] getByteArray(ByteBuffer buffer) {
        int dstSize = buffer.limit() - buffer.position();
        byte[] dst = new byte[dstSize];
        buffer.get(dst, 0, dstSize);
        return dst;
    }

    @Override
    public void acquireLock(StaticBuffer key, StaticBuffer column, StaticBuffer expectedValue, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public KeyIterator getKeys(KeyRangeQuery query, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public KeyIterator getKeys(SliceQuery query, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public String getName() {
        return "DUMMY_NAME";
    }

    @Override
    public void close() throws BackendException {
        sfconn.close();
    }
}
