package com.sstech.janusgraph.diskstorage.snowflake.keycolumnvalue;

import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.common.AbstractStoreTransaction;

public class SnowFlakeStoreTransaction extends AbstractStoreTransaction {
    public SnowFlakeStoreTransaction(BaseTransactionConfig config) {
        super(config);
    }
}
