// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.sstech.janusgraph.diskstorage.snowflake;

import com.google.common.base.Preconditions;
import com.google.common.util.concurrent.ThreadFactoryBuilder;
import com.sstech.janusgraph.common.exceptions.SnowFlakeConnectionException;
import com.sstech.janusgraph.common.utils.LogUtils;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnectionManager;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.common.AbstractStoreManager;
import org.janusgraph.diskstorage.configuration.ConfigOption;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.keycolumnvalue.KeyRange;
import org.janusgraph.diskstorage.keycolumnvalue.StandardStoreFeatures;
import org.janusgraph.diskstorage.keycolumnvalue.StoreFeatures;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVMutation;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStoreManager;
import org.janusgraph.diskstorage.util.time.TimestampProvider;
import org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.time.Instant;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.*;

public class SnowFlakeStoreManager extends AbstractStoreManager implements OrderedKeyValueStoreManager {
    private static final Logger log = LoggerFactory.getLogger(SnowFlakeStoreManager.class);
    private boolean isLogged = true;

    private SnowFlakeConnectionManager connectionManager;
    private SnowFlakeConnection connection;

    private Map<String, SnowFlakeKeyValueStore> stores;
    private SnowFlakeTransaction tx;
    private Map<String, SnowFlakeDatabase> databases = new HashMap<>();

    private String dbName;
    private String schemaName;

    private final ExecutorService executorService;

    //  protected Environment environment;
    protected StoreFeatures features;
    BaseTransactionConfig cfg = new BaseTransactionConfig() {
        @Override
        public Instant getCommitTime() {
            return null;
        }

        @Override
        public void setCommitTime(Instant instant) {

        }

        @Override
        public boolean hasCommitTime() {
            return false;
        }

        @Override
        public TimestampProvider getTimestampProvider() {
            return null;
        }

        @Override
        public String getGroupName() {
            return null;
        }

        @Override
        public boolean hasGroupName() {
            return false;
        }

        @Override
        public <V> V getCustomOption(ConfigOption<V> configOption) {
            return null;
        }

        @Override
        public Configuration getCustomOptions() {
            return null;
        }
    };

    public SnowFlakeStoreManager(Configuration configuration) throws BackendException {
        super(configuration);

        // TODO: Also read in Graph name & specify it to be appended to properties file
        // TODO: Override SnowFlakeConnectionManager with Properties object as input
        // todo: Done

        Properties connectionProperties = setupConnectionProperties(configuration);

        connectionManager = SnowFlakeConnectionManager.getInstance(connectionProperties);

        try {
            connectionManager.connect();
            connection = connectionManager.getConnection();
        } catch (SnowFlakeConnectionException e) {
            e.printStackTrace();
            throw new PermanentBackendException("Unable to connect to SnowFlake DB");
        }

        // Create SnowFlake connection object
        stores = new HashMap<>();

        this.executorService = new ThreadPoolExecutor(2,
            5,
            1,
            TimeUnit.MINUTES,
            new LinkedBlockingQueue<>(),
            new ThreadFactoryBuilder()
                .setDaemon(true)
                .setNameFormat("SnowFlakeStoreManager[%02d]")
                .build());

        features = new StandardStoreFeatures.Builder()
            .orderedScan(true)
            .keyConsistent(GraphDatabaseConfiguration.buildGraphConfiguration())
            .locking(true)
            .keyOrdered(true)
            .supportsInterruption(false)
            .optimisticLocking(false)
            .build();
    }

    /**
     * review this
     * private void initialize(int cachePercent) throws BackendException {
     * try {
     * EnvironmentConfig envConfig = new EnvironmentConfig();
     * envConfig.setAllowCreate(true);
     * envConfig.setTransactional(transactional);
     * envConfig.setCachePercent(cachePercent);
     * <p>
     * if (batchLoading) {
     * envConfig.setConfigParam(EnvironmentConfig.ENV_RUN_CHECKPOINTER, "false");
     * envConfig.setConfigParam(EnvironmentConfig.ENV_RUN_CLEANER, "false");
     * }
     * <p>
     * //Open the environment
     * environment = new Environment(directory, envConfig);
     * <p>
     * <p>
     * } catch (DatabaseException e) {
     * throw new PermanentBackendException("Error during Snowflake initialization: ", e);
     * }
     * <p>
     * }
     **/

    private void initializeDatabase() throws Exception {
        SnowFlakeDatabase db = new SnowFlakeDatabase("", this.getTransaction());
        OperationStatus result = db.getTables(dbName, schemaName);

        @SuppressWarnings("unchecked")
        List<String> tables = (List<String>) result.getResult();

        for (String tblName : tables) {
            SnowFlakeDatabase sdb = new SnowFlakeDatabase(tblName, getTransaction());
            sdb.close();
        }
        db.close();
    }

    private Properties setupConnectionProperties(Configuration graphConfiguration) throws BackendException {
        String clusterFilePath = graphConfiguration.get(CLUSTER_FILE_PATH);
        String graphName = graphConfiguration.get(GRAPH);
        String schema = graphConfiguration.get(SCHEMA);

        this.dbName = graphName;
        this.schemaName = schema;

        Properties connectionProperties;

        try {
            File configFile = new File(clusterFilePath);
            System.out.println("Reading configuration from" + configFile.getAbsolutePath());
            System.out.println(configFile.exists());
            InputStream inputHandler = new FileInputStream(clusterFilePath);
            connectionProperties = new Properties();

            connectionProperties.load(inputHandler);

        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException(LogUtils.stringFormatter("Unable to locate Cluster Connection File with SnowFlake credentials (%s)", clusterFilePath));
        }

        connectionProperties.put("DB", graphName);
        connectionProperties.put("schema", schema);

        return connectionProperties;
    }

    @Override
    public StoreFeatures getFeatures() {
        return features;
    }

    @Override
    public List<KeyRange> getLocalKeyPartition() throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public SnowFlakeTransaction beginTransaction(final BaseTransactionConfig txCfg) throws BackendException {
        if (tx == null) {
            tx = new SnowFlakeTransaction(txCfg, this, connection);
            if (isLogged) {
                log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
            }
        }

        return tx;
    }

    @Override
    public SnowFlakeKeyValueStore openDatabase(String name) throws BackendException {
        Preconditions.checkNotNull(name);
        if (stores.containsKey(name)) {
            return stores.get(name);
        } else {
            try {
                SnowFlakeDatabase db = new SnowFlakeDatabase(name, connection);

                if (isLogged)
                    log.info(LogUtils.stringFormatter("Opened database %s", name));

                SnowFlakeKeyValueStore store = new SnowFlakeKeyValueStore(db);

                if (isLogged)
                    log.info(LogUtils.stringFormatter("Opened store for %s", name));

                databases.put(name, db);
                stores.put(name, store);

                return store;

            } catch (Exception e) {
                throw new PermanentBackendException(e);
            }
        }

    }

    @Override
    public void mutateMany(Map<String, KVMutation> mutations, StoreTransaction txh) throws BackendException {
        for (Map.Entry<String, KVMutation> mutation : mutations.entrySet()) {
            SnowFlakeKeyValueStore store = openDatabase(mutation.getKey());
            KVMutation mutationValue = mutation.getValue();

            if (!mutationValue.hasAdditions() && !mutationValue.hasDeletions()) {
                log.warn(LogUtils.stringFormatter("Empty mutation set for %s, doing nothing", mutation.getKey()));
            } else {
                log.warn(LogUtils.stringFormatter("Mutating %s", mutation.getKey()));
            }

            if (mutationValue.hasAdditions()) {
                for (KeyValueEntry entry : mutationValue.getAdditions()) {
                    store.insert(entry.getKey(), entry.getValue(), txh);
                    log.info(LogUtils.stringFormatter("Insertion on %s: %s", mutation.getKey(), entry.toString()));
                }
            }
            if (mutationValue.hasDeletions()) {
                for (StaticBuffer del : mutationValue.getDeletions()) {
                    store.delete(del, txh);
                    log.info(LogUtils.stringFormatter("Deletion on %s: %s", mutation.getKey(), del.toString()));
                }
            }
        }
    }

    void removeDatabase(SnowFlakeKeyValueStore db) {
        if (!stores.containsKey(db.getName())) {
            throw new IllegalArgumentException("Tried to remove an unkown database from the storage manager");
        }
        String name = db.getName();
        stores.remove(name);
        log.info(LogUtils.stringFormatter("Removed database %s", name));
    }

    ExecutorService getExecutorService() {
        return this.executorService;
    }

    @Override
    public void close() {

        databases.forEach((name, database) -> {
            try {
                database.close();
                log.info("Closed " + name);
            } catch (PermanentBackendException e) {
                log.error(LogUtils.stringFormatter("Error closing %s ", name));
            }
        });

        stores.forEach((name, store) -> {
            try {
                store.close();
                log.info(LogUtils.stringFormatter("Closed store %s ", name));
            } catch (BackendException e) {
                log.error(LogUtils.stringFormatter("Error closing connection to store %s ", name));
            }
        });

        this.tx.close();
        this.executorService.shutdownNow();
    }

    @Override
    public void clearStorage() throws BackendException {
        if (!stores.isEmpty()) {
            throw new IllegalStateException("Cannot delete store, since database is open: " + stores.keySet().toString());
        }
        //list the tables from snowflake db and remove the respective table..
        // - in snowflake db - 2 functions, list table and remove table.
        SnowFlakeDatabase db = null;
        try {
            db = new SnowFlakeDatabase("", this.getTransaction());///change it to transaction as required.

            OperationStatus tableList = db.getTables(dbName, schemaName);

            if (tableList.getStatus()) {
		@SuppressWarnings("unchecked")
                List<String> tables = (List<String>) tableList.getResult();

                for (final String tblName : tables) {
                    OperationStatus status = db.removeTable(schemaName, tblName);
                    if (status.getStatus()) {
                        log.info(LogUtils.stringFormatter("Removed database %s (clearStorage)", db.getTableName()));
                    } else {
                        log.error(LogUtils.stringFormatter("Couldn't remove table %s from snowflake backend", tblName));
                    }
                }
            } else {
                throw new PermanentBackendException("Unable to fetch table list from snowflake due to error");
            }
        } catch (IOException e) {
            log.error("Couldnt connect to snowflake as log file couldn't be created");
        } catch (SnowFlakeConnectionException e) {
            log.error("Couldnt connect to snowflake");
        }
        close();
    }

    @Override
    public boolean exists() throws BackendException {
        //  TODO: check table names not empty here..
        return true;
    }

    @Override
    public String getName() {
        return getClass().getSimpleName() + (connection == null ? "" : connection.getURL());
    }

    private static class TransactionBegin extends Exception {
        private static final long serialVersionUID = 1L;

        private TransactionBegin(String msg) {
            super(msg);
        }
    }

    public SnowFlakeTransaction getTransaction() throws BackendException {
        if (tx == null) {
            tx = new SnowFlakeTransaction(cfg, this, connection);

            if (isLogged) {
                log.info(LogUtils.stringFormatter("SnowFlakeTransaction tx created %s", tx.toString()));
            }
        }
        return tx;
    }
}
