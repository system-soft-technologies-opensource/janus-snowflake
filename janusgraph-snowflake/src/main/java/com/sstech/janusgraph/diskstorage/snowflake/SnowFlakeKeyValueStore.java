// Copyright 2019 SystemSoft Technologies
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package com.sstech.janusgraph.diskstorage.snowflake;

import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeCursor;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.StoreTransaction;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KVQuery;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeySelector;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.OrderedKeyValueStore;
import org.janusgraph.diskstorage.util.RecordIterator;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;


public class SnowFlakeKeyValueStore implements OrderedKeyValueStore {
    Logger log = LoggerFactory.getLogger(SnowFlakeKeyValueStore.class);
    private boolean isLogged = true;
    private final String name;
    private SnowFlakeStoreManager storeManager;
    private SnowFlakeDatabase db;
    private boolean isOpen;

    private static final StaticBuffer.Factory<byte[]> ENTRY_FACTORY = (array, offset, limit) -> {
        final byte[] bArray = new byte[limit - offset];
        System.arraycopy(array, offset, bArray, 0, limit - offset);
        return bArray;
    };

    public SnowFlakeKeyValueStore(SnowFlakeDatabase database) {


        name = database.getTableName();
        db = database;
    }


    @Override
    public String getName() {
        return name;
    }

    @Override
    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh) throws BackendException {
        insert(key, value, txh, true);
    }

    // constructor of  --- insert and get ...
    // get operation from table system_properties, key value, to_binary ...decode to base64
    // base64 string to decode in to byte array
    // byte array to static buffer.
    public void insert(StaticBuffer key, StaticBuffer value, StoreTransaction txh, boolean allowOverwrite) throws BackendException {

        try {
            OperationStatus status;

            if (isLogged) {
                log.info(String.format("db=%s, op=insert, tx=%s", name, txh));
            }

            byte[] keyByte = key.as(ENTRY_FACTORY);
            byte[] valueByte = value.as(ENTRY_FACTORY);

            if (allowOverwrite)
                status = db.put(keyByte, valueByte);
            else
                status = db.putNotOverWrite(keyByte, valueByte);
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public RecordIterator<KeyValueEntry> getSlice(KVQuery query, StoreTransaction txh) throws BackendException {
        if (isLogged) {
            log.info(String.format("db=%s, op=getSlice, tx=%s", name, txh));
        }

        final StaticBuffer keyStart = query.getStart();
        final StaticBuffer keyEnd = query.getEnd();
        final KeySelector selector = query.getKeySelector();

        final List<KeyValueEntry> result = new ArrayList<>();
        final byte[] foundKey = keyStart.as(ENTRY_FACTORY);
        SnowFlakeCursor cursor;

        try {
            cursor = db.openCursor();
        } catch (IOException e) {
            e.printStackTrace();
            throw new PermanentBackendException("Unable to open Cursor as Log file creation failed");
        }

        OperationStatus status = new OperationStatus(true);

        //Iterate until given condition is satisfied or end of records
        ResultSet res;
        res = cursor.getSearchKey(foundKey);
        try {
            while (res.next()) {
                StaticBuffer key = getBuffer(foundKey);

                if (key.compareTo(keyEnd) >= 0)
                    break;

                if (selector.include(key)) {
                    String encodedData = res.getString("VALUE");

                    byte[] foundData = Base64.getDecoder().decode(encodedData);

                    result.add(new KeyValueEntry(key, getBuffer(foundData)));
                }

                if (selector.reachedLimit())
                    break;
            }
        } catch (SQLException e) {
            throw new PermanentBackendException("Invalid iteration over records");
        } catch (NullPointerException e) {
            throw new PermanentBackendException(e);
        }

        if (isLogged) {
            log.info(String.format("db=%s, op=getSlice, key=%s, tx=%s, resultcount=%s", name,
                Base64.getEncoder().encodeToString(foundKey), txh, result.size()));
        }

        return new RecordIterator<KeyValueEntry>() {
            private final Iterator<KeyValueEntry> entries = result.iterator();

            @Override
            public boolean hasNext() {
                return entries.hasNext();
            }

            @Override
            public KeyValueEntry next() {
                return entries.next();
            }

            @Override
            public void close() {
            }

            @Override
            public void remove() {
                throw new UnsupportedOperationException();
            }
        };
    }

    @Override
    public Map<KVQuery, RecordIterator<KeyValueEntry>> getSlices(List<KVQuery> queries, StoreTransaction txh) throws BackendException {
        throw new UnsupportedOperationException();
    }

    @Override
    public void delete(StaticBuffer key, StoreTransaction txh) throws BackendException {

        if (isLogged) {
            log.info(String.format("db=%s, op=delete, tx=%s, key=%s", name, txh, key.toString()));
        }

        OperationStatus status;
        try {
            if (isLogged) {
                log.info(String.format("db=%s, op=deleteSuccess, tx=%s, key=%s", name, txh, key.toString()));
            }

            byte[] keyByte = key.as(ENTRY_FACTORY);

            status = db.delete(keyByte);

            if (!status.getStatus()) {
                throw new PermanentBackendException("Could not remove: " + status);
            }
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public StaticBuffer get(StaticBuffer key, StoreTransaction txh) throws BackendException {
        try {
            if (isLogged) {
                log.info(String.format("db=%s, op=get, tx=%s, key=%s", name, txh, key.toString()));
            }
            // Convert StaticBuffer to byte array
            byte[] keyByte = key.as(ENTRY_FACTORY);
            // Encode byte array to Base64.
            String encodedkey = Base64.getEncoder().encodeToString(keyByte);
            // Do DB Operation using Base64 Key.
            OperationStatus status = db.get(encodedkey);

            if (status.getStatus()) {
                ArrayList<String> result = status.getResultAsList("VALUE");
                String dbEncodedString = result.get(0);
                byte[] entry = Base64.getDecoder().decode(dbEncodedString);
                return getBuffer(entry);
            } else
                return getBuffer(null);

        } catch (Exception e) {
            e.printStackTrace();
            throw new PermanentBackendException(e);
        }
    }

    public StaticBuffer get(StaticBuffer key, StoreTransaction txh, String columnName) throws BackendException {
        try {
            if (isLogged) {
                log.info(String.format("db=%s, op=get, tx=%s, key=%s", name, txh, key.toString()));
            }
            // Convert StaticBuffer to byte array
            byte[] keyByte = key.as(ENTRY_FACTORY);
            // Encode byte array to Base64.
            String encodedkey = Base64.getEncoder().encodeToString(keyByte);
            // Do DB Operation using Base64 Key.
            OperationStatus status = db.get(encodedkey);
            if (status.getStatus()) {
                ArrayList<String> result = status.getResultAsList(columnName);
                String dbEncodedString = result.get(0);
                // Decode the return Base64 String to byte array
                byte[] entry = Base64.getDecoder().decode(dbEncodedString);
                // Convert Byte Array to StaticBuffer.
                return getBuffer(entry);
            } else
                return getBuffer(null);

        } catch (Exception e) {
            e.printStackTrace();
            throw new PermanentBackendException(e);
        }
    }

    @Override
    public boolean containsKey(StaticBuffer key, StoreTransaction txh) throws BackendException {
        return get(key, txh) != null;
    }

    @Override
    public void acquireLock(StaticBuffer key, StaticBuffer expectedValue, StoreTransaction txh) throws BackendException {
        if (this.storeManager.getTransaction() == null) {
            if (isLogged) {
                log.warn("Attempt to acquire lock with transactions disabled");
            }
        } //else we need no locking
    }

    @Override
    public synchronized void close() throws BackendException {
        try {
            if (isOpen) db.close();
        } catch (Exception e) {
            throw new PermanentBackendException(e);
        }
        isOpen = false;
    }

    private static StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

}
