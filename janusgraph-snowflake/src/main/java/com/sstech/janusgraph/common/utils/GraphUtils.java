package com.sstech.janusgraph.common.utils;

import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.util.StandardBaseTransactionConfig;
import org.janusgraph.diskstorage.util.time.TimestampProviders;

public class GraphUtils {

    public static String BerkleyJE_Graph_Path = GraphUtils.generateBDBGraphPath();
    public static String SnowFlake_Cluster_File = GraphUtils.generateSnowFlakeClusterFilePath();

    static private String generateBDBGraphPath() {
        if (OSUtils.isWindows()) {
            return "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\graph1";
        }
        else if (OSUtils.isUnix()) {
            return "/tmp/graph";
        }
        else
            return "/tmp/graph";
    }

    static private String generateSnowFlakeClusterFilePath() {
        if (OSUtils.isWindows()) {
            return "sf-cluster-credentials.properties";
        }
        else if (OSUtils.isUnix()) {
            return "sf-cluster-credentials.properties";
        }
        else
            return "sf-cluster-credentials.properties";
    }

    public static BaseTransactionConfig getEmptyTxConfig() {
        return StandardBaseTransactionConfig.of(TimestampProviders.MICRO);
    }

}
