package com.sstech.janusgraph;

import com.sstech.janusgraph.diskstorage.snowflake.TestSnowFlakeKeyValueStore;
import com.sstech.janusgraph.snowflake.TestSnowFlakeDatabase;
import org.junit.runner.JUnitCore;
import org.junit.runner.Result;
import org.junit.runner.notification.Failure;

public class TestRunner {
   public static void main(String[] args) {
      Result result = JUnitCore.runClasses(TestSnowFlakeDatabase.class);
		
      for (Failure failure : result.getFailures()) {
         System.out.println(failure.toString());
      }
		
      System.out.println(result.wasSuccessful());
   }
}  