package com.sstech.janusgraph.common.comparision.datafetcher;

import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageSetup;
import com.sstech.janusgraph.common.utils.GraphUtils;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeKeyValueStore;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJEStoreManager;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;

import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.*;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.VERSION;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.STORAGE_BACKEND;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.buildGraphConfiguration;

public class SnowFlakeDataFetcher {
    private String tableName = "edgestore";
    private String graphName = "GRAPHDBTEST";
    private String clusterFilePath;
    private SnowFlakeStoreManager manager;
    private SnowFlakeTransaction tx;
    private SnowFlakeKeyValueStore store;
    private SnowFlakeDatabase db;

    public SnowFlakeDataFetcher() {
//        clusterFilePath = SnowFlakeDBStorageSetup.getClusterFilePath();
    }

    private Configuration getGraphConfiguration() {
        return SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration();
    }

    public void initializeDataFetchEngine() throws Exception {

        initializeStoreManager();
        initializeTransaction();
        initializeDatabase();
        initializeStore();
    }

    public void closeResources() throws PermanentBackendException {
        try {
            store.close();
            db.close();
            tx.commit();
            manager.close();
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to close DB or store instance");
        }
    }

    private void initializeStoreManager() throws PermanentBackendException {
        try {
            manager = new SnowFlakeStoreManager(getGraphConfiguration());
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to open connection to BerkleyDB");
        }
    }

    private void initializeTransaction() throws BackendException {
        tx = manager.getTransaction();
    }

    private void initializeDatabase() throws Exception {
        db = new SnowFlakeDatabase(tableName, tx);
    }

    private void initializeStore() {
        store = new SnowFlakeKeyValueStore(db);
    }

    private byte[] getByteArray(String encodedString) {
        return Base64.getDecoder().decode(encodedString);
    }

    private StaticBuffer getBuffer(byte[] entry) {
        return new StaticArrayBuffer(entry);
    }

    private String getEncodedString(byte[] entry) {
        return Base64.getEncoder().encodeToString(entry);
    }

    public KeyValueEntry getKeyValueEntry(byte[] key, byte[] value) throws SQLException {
        String encodedKey = getEncodedString(key);
        String encodedValue = getEncodedString(value);

        return db.get(encodedKey, encodedValue);
    }

    public List<KeyValueEntry> getTopKeyValueEntries(Integer top) throws SQLException {
        final List<KeyValueEntry> resultsKeyValueEntry = new ArrayList<>();

        OperationStatus result = db.getAll(top);

        ResultSet resultSet = (ResultSet) result.getResult();

        while (resultSet.next()) {
            String keyEncoded = resultSet.getString("KEY");
            String valueEncoded = resultSet.getString("VALUE");

            byte[] byteKey = getByteArray(keyEncoded);
            byte[] byteValue = getByteArray(valueEncoded);

            StaticBuffer key = getBuffer(byteKey);
            StaticBuffer value = getBuffer(byteValue);

            KeyValueEntry entry = new KeyValueEntry(key, value);

            resultsKeyValueEntry.add(entry);
        }

        return resultsKeyValueEntry;
    }

    public KeyValueEntry getMatchingKeyValueEntries(byte[] key) throws SQLException {
        KeyValueEntry resultsKeyValueEntry = null;

        OperationStatus result = db.get(key);

        ResultSet resultSet = (ResultSet) result.getResult();

        while (resultSet.next()) {
            String keyEncoded = resultSet.getString("KEY");
            String valueEncoded = resultSet.getString("VALUE");

            byte[] byteKey = getByteArray(keyEncoded);
            byte[] byteValue = getByteArray(valueEncoded);

            StaticBuffer keySB = getBuffer(byteKey);
            StaticBuffer value = getBuffer(byteValue);

            KeyValueEntry entry = new KeyValueEntry(keySB, value);
            resultsKeyValueEntry = entry;
        }

        return resultsKeyValueEntry;
    }
}
