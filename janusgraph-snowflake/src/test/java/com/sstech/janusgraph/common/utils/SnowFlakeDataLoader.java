package com.sstech.janusgraph.common.utils;

import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageSetup;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeKeyValueStore;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.snowflakedb.SnowFlakeDatabase;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;

import java.util.ArrayList;
import java.util.List;

import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.*;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.VERSION;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.STORAGE_BACKEND;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.buildGraphConfiguration;

public class SnowFlakeDataLoader {
    // This class gets StaticBuffer Key & Value and loads into specified Table

    private String tableName = "edgestore";
    private String graphName = "GRAPHDBTEST";
    private String clusterFilePath;
    private SnowFlakeStoreManager manager;
    private SnowFlakeTransaction tx;
    private SnowFlakeKeyValueStore store;
    private SnowFlakeDatabase db;

    public SnowFlakeDataLoader() {
        clusterFilePath = SnowFlakeDBStorageSetup.getClusterFilePath();
    }

    private Configuration getGraphConfiguration() {
        return buildGraphConfiguration()
            .set(STORAGE_BACKEND, "com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager")
            .set(GRAPH, graphName)
            .set(SCHEMA, "GRAPHDBTEST1")
            .set(CLUSTER_FILE_PATH, clusterFilePath)
            .set(VERSION, "3.10.3");
    }

    public void initializeDataLoadEngine() throws Exception {

        initializeStoreManager();
        initializeTransaction();
        initializeDatabase();
        initializeStore();
    }

    public void closeResources() throws PermanentBackendException {
        try {
            store.close();
            db.close();
            tx.commit();
            manager.close();
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to close DB or store instance");
        }
    }

    private void initializeStoreManager() throws PermanentBackendException {
        try {
            manager = new SnowFlakeStoreManager(getGraphConfiguration());
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to open connection to BerkleyDB");
        }
    }

    private void initializeTransaction() throws BackendException {
        tx = manager.getTransaction();
    }

    private void initializeDatabase() throws Exception {
        db = new SnowFlakeDatabase(tableName, tx);
    }

    private void initializeStore() {
        store = new SnowFlakeKeyValueStore(db);
    }

    public OperationStatus insert(StaticBuffer key, StaticBuffer value) {
        OperationStatus res = new OperationStatus(true);

        try {
            store.insert(key, value, tx);
        } catch (BackendException e) {
            res.setOperationStatus(false);
        }

        return res;
    }

    public OperationStatus insertArray(StaticBuffer[] keys, StaticBuffer[] values) {
        OperationStatus status = new OperationStatus(false);

        for (int i = 0; i < keys.length; i++) {
            StaticBuffer key = keys[i];
            StaticBuffer value = values[i];

            OperationStatus res = this.insert(key, value);

            if (!res.getStatus()) {
                status.setOperationStatus(res.getStatus());
            }
        }
        return status;
    }

    public OperationStatus insert(KeyValueEntry keyValueEntry) {
        return this.insert(keyValueEntry.getKey(), keyValueEntry.getValue());
    }

    public OperationStatus insertArray(KeyValueEntry[] keyValueEntries) {
        OperationStatus status = new OperationStatus(true);

        for (KeyValueEntry keyValueEntry : keyValueEntries) {
            OperationStatus res = this.insert(keyValueEntry.getKey(), keyValueEntry.getValue());

            if (!res.getStatus()) {
                status.setOperationStatus(res.getStatus());
            }
        }
        return status;
    }

    public OperationStatus insertArray(List<KeyValueEntry> keyValueEntries) {
        OperationStatus status = new OperationStatus(true);

        for (KeyValueEntry keyValueEntry : keyValueEntries) {
            OperationStatus res = this.insert(keyValueEntry.getKey(), keyValueEntry.getValue());

            if (!res.getStatus()) {
                status.setOperationStatus(res.getStatus());
            }
        }
        return status;
    }
}
