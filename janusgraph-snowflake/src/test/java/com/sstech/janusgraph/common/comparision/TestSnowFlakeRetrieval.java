package com.sstech.janusgraph.common.comparision;

import com.sstech.janusgraph.common.comparision.datafetcher.BerkleyJEDataFetcher;
import com.sstech.janusgraph.common.comparision.datafetcher.SnowFlakeDataFetcher;
import com.sstech.janusgraph.common.storage.BerkeleyDBStorageSetup;
import com.sstech.janusgraph.common.utils.GraphUtils;
import com.sstech.janusgraph.common.utils.SnowFlakeDataLoader;
import com.sstech.janusgraph.snowflake.utils.OperationStatus;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.junit.*;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


public class TestSnowFlakeRetrieval {
    /*
    This Class takes into consideration that already a BerkleyDB Env(JanusGraph) is defined and exists at the Path specified.
    Is then initialized BerkleyDBReader class which reads in data from the table specified. It fetches top 20 records from edgestore DB in BDB

    We then follow it up, by making use of custom SnowFlakeDataLoader class which loads the list of records (KeyValueEntry)
    into SnowFlake DB by making use of store.insert(SB key, SB value).
    TODO: We need to do this process for all the tables once GraphOfGods is loaded successfully and do g.V().count() for checking.

    NOTE: The data loading step is done once only. Hence it is commented out in this part of code as data loading is complete.

    Once data is loaded, we then do Read Operation using store.get() to check for data consistency.

    The following assertions are done:
    1: For each BerkleyDB record fetched, in step 1, we generate KeyValueEntry and query using the same on SnowFlakeDB.
        As assertion on record fetched against SnowFlake and initial BerkleyDB is done.
    2: For each BerkleyDB record fetched, in step 1, we generate KeyValueEntry and query using the same on SnowFlakeDB.
        As assertion on keys fetched against SnowFlake and initial BerkleyDB is done.
    3: For each BerkleyDB record fetched, in step 1, we generate KeyValueEntry and query using the same on SnowFlakeDB.
        As assertion on values fetched against SnowFlake and initial BerkleyDB is done.
    */

    private String graphPath;
    private BerkleyJEDataFetcher dataFetcher;
    private SnowFlakeDataLoader dataLoader;
    private SnowFlakeDataFetcher sfDataFetcher;

    private List<KeyValueEntry> bdbRecords;

    private static final StaticBuffer.Factory<byte[]> ENTRY_FACTORY = (array, offset, limit) -> {
        final byte[] bArray = new byte[limit - offset];
        System.arraycopy(array, offset, bArray, 0, limit - offset);
        return bArray;
    };

    public TestSnowFlakeRetrieval() throws PermanentBackendException {

    }

    @Before
    public void loadData() throws Exception {
        System.out.println("Im setting up initial resources");

//        graphPath = "/root/data/graph";
//        graphPath = "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\graph";
        graphPath = BerkeleyDBStorageSetup.getDataPath();
        initializeBDBDataReader();
        initializeSnowFlakeDataLoader();
        initializeSnowFlakeDataReader();
        dataLoaderForSnowFlake();
    }

    private void dataLoaderForSnowFlake() {
        bdbRecords = loadBerkleyDBRecords();
//        loadDataIntoSnowFlake();
    }

    private void initializeBDBDataReader() throws PermanentBackendException {
        dataFetcher = new BerkleyJEDataFetcher(graphPath);
        dataFetcher.initializeDataFetchEngine();
    }

    private void initializeSnowFlakeDataLoader() throws Exception {
        dataLoader = new SnowFlakeDataLoader();
        dataLoader.initializeDataLoadEngine();
    }

    private void initializeSnowFlakeDataReader() throws Exception {
        sfDataFetcher = new SnowFlakeDataFetcher();
        sfDataFetcher.initializeDataFetchEngine();
    }

    private void loadDataIntoSnowFlake() {
        OperationStatus res = dataLoader.insertArray(bdbRecords);

    }

    private KeyValueEntry getRecord(KeyValueEntry entry) throws SQLException {
        StaticBuffer key = entry.getKey();
        StaticBuffer value = entry.getValue();

        byte[] keyByte = key.as(ENTRY_FACTORY);
        byte[] valueByte = value.as(ENTRY_FACTORY);

        return sfDataFetcher.getKeyValueEntry(keyByte, valueByte);
    }

    private KeyValueEntry getRecord(StaticBuffer key) throws SQLException {
        byte[] keyByte = key.as(ENTRY_FACTORY);

        return sfDataFetcher.getMatchingKeyValueEntries(keyByte);
    }

    private List<KeyValueEntry> loadBerkleyDBRecords() {
        int recordLimit = 20;
        List<KeyValueEntry> records = dataFetcher.getTopKeyValueEntries(recordLimit);
        return records;
    }

    @Test
    public void testConsistentDataAcrossSources() throws SQLException {
        // This Test tests for data consistency across DBD and SnowFlake
        // Reads in data from edgestore table in DDB
        // Inserts data into SnowFlake after extracting from BDB
        // Then fetches from SnowFlake using KeyValue Entry and does an assertion

        // Considers that the first two steps are done. i.e. Loading from BDB and Loading in SF is done.

        List<KeyValueEntry> sfRecords = new ArrayList<>();

        bdbRecords.forEach( record -> {
            KeyValueEntry sfRecord = null;
            try {
                sfRecord = getRecord(record);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sfRecords.add(sfRecord);
        });

        for (int i = 0; i < bdbRecords.size(); i++) {
            KeyValueEntry bdbRecord = bdbRecords.get(i);
            KeyValueEntry sfRecord = sfRecords.get(i);

            Assert.assertEquals(bdbRecord.getKey(), sfRecord.getKey());
            Assert.assertEquals(bdbRecord.getValue(), sfRecord.getValue());
        }
    }

    @Test
    public void testConsistentKeyAcrossSources() {
        List<KeyValueEntry> sfRecords = new ArrayList<KeyValueEntry>();

        bdbRecords.forEach( record -> {
            KeyValueEntry sfRecord = null;
            try {
                sfRecord = getRecord(record.getKey());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sfRecords.add(sfRecord);
        });

        for (int i = 0; i < bdbRecords.size(); i++) {
            KeyValueEntry bdbRecord = bdbRecords.get(i);
            KeyValueEntry sfRecord = sfRecords.get(i);

            Assert.assertEquals(bdbRecord.getKey(), sfRecord.getKey());
        }
    }

    @Test
    public void testConsistentValueAcrossSources() {
        List<KeyValueEntry> sfRecords = new ArrayList<KeyValueEntry>();

        bdbRecords.forEach( record -> {
            KeyValueEntry sfRecord = null;
            try {
                sfRecord = getRecord(record.getKey());
            } catch (SQLException e) {
                e.printStackTrace();
            }
            sfRecords.add(sfRecord);
        });

        for (int i = 0; i < bdbRecords.size(); i++) {
            KeyValueEntry bdbRecord = bdbRecords.get(i);
            KeyValueEntry sfRecord = sfRecords.get(i);

            Assert.assertEquals(bdbRecord.getValue(), sfRecord.getValue());
        }
    }

    @After
    public void closeResources() throws Exception {
        if (dataFetcher != null) {
            dataFetcher.closeResources();
        }
        if (dataLoader != null) {
            dataLoader.closeResources();
        }
        if (sfDataFetcher != null) {
            sfDataFetcher.closeResources();
        }
    }
}
