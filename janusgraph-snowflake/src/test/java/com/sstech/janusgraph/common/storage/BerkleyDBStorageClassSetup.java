package com.sstech.janusgraph.common.storage;

import com.sleepycat.je.*;
import com.sstech.janusgraph.common.utils.GraphUtils;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJEKeyValueStore;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJEStoreManager;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJETx;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.util.StandardBaseTransactionConfig;
import org.janusgraph.diskstorage.util.time.TimestampProviders;

import java.io.File;

import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.*;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.DROP_ON_CLEAR;

public class BerkleyDBStorageClassSetup {

    // BerkleyDB connection parameters
    private String dataPath = BerkeleyDBStorageSetup.getDataPath();
    private File directory = new File(dataPath);
    private Environment bdbEnvironment;
    private Database bdbDB;
    private BerkeleyJEStoreManager bdbManager;
    private BerkeleyJEKeyValueStore bdbStore;
    private Cursor bdbCur;
    private Transaction bdbTx;
    private BerkeleyJETx bdbTxh;
    private String tableName;

    public BerkleyDBStorageClassSetup(String tableName) {
        this.tableName = tableName;
    }

    public void initializeBDBEngine() throws PermanentBackendException {

        initializeBDBEnvironment();
        initializeBDBDatabase();
        initializeBDBStoreManager();
//        initializeBDBTransaction();
        initializeBDBTxh();
        initializeBDBStore();
        openBDBCursor();
    }

    private void initializeBDBEnvironment() {
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        bdbEnvironment = new Environment(directory, envConfig);
    }

    private void initializeBDBDatabase() {
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setReadOnly(false);
        dbConfig.setAllowCreate(true);
        dbConfig.setTransactional(true);
        bdbDB = bdbEnvironment.openDatabase(null, tableName, dbConfig);
    }

    private void initializeBDBStoreManager() throws PermanentBackendException {
        try {
            bdbManager = new BerkeleyJEStoreManager(getBDBGraphConfiguration());
//            bdbManager.openDatabase(tableName);
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to open connection to BerkleyDB");
        }

    }

    private Configuration getBDBGraphConfiguration() {

        return BerkeleyDBStorageSetup.getBerkeleyJEConfiguration();
    }

    private void initializeBDBTransaction() {
        TransactionConfig txnConfig = new TransactionConfig();
        bdbTx = bdbEnvironment.beginTransaction(null, txnConfig);
    }

//    private BaseTransactionConfig getTxConfig() {
//        return StandardBaseTransactionConfig.of(TimestampProviders.MICRO);
//    }

    private void initializeBDBTxh() {
//        Configuration cfg = getBDBGraphConfiguration();
        TransactionConfig txnConfig = new TransactionConfig();
        bdbTx = bdbEnvironment.beginTransaction(null, txnConfig);
        bdbTxh = new BerkeleyJETx(bdbTx, LockMode.DEFAULT, GraphUtils.getEmptyTxConfig());
    }

    private void initializeBDBStore() {
        bdbStore = new BerkeleyJEKeyValueStore(tableName, bdbDB, bdbManager);
    }

    private void openBDBCursor() {
        bdbCur = bdbDB.openCursor(bdbTx, null);
    }

    public void closeBDBResources() throws PermanentBackendException {
        try {
            bdbCur.close();
//            bdbStore.close();
            bdbTx.commit();
            bdbManager.close();
            bdbDB.close();
            bdbEnvironment.close();
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to close DB or store instance");
        }
    }

    public BerkeleyJEStoreManager getManager() {
        return bdbManager;
    }

    public BerkeleyJETx getTransaction() {
        return bdbTxh;
    }

    public BerkeleyJEKeyValueStore getStore() {
        return bdbStore;
    }

    public Cursor getCursor() {
        return bdbCur;
    }

}
