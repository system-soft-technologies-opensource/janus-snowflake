package com.sstech.janusgraph.common.comparision.datafetcher;

import com.sleepycat.je.*;
import com.sleepycat.je.utilint.Stat;
import com.sstech.janusgraph.common.storage.BerkeleyDBStorageSetup;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJEKeyValueStore;
import org.janusgraph.diskstorage.berkeleyje.BerkeleyJEStoreManager;
import org.janusgraph.diskstorage.configuration.ConfigNamespace;
import org.janusgraph.diskstorage.configuration.ConfigOption;
import org.janusgraph.diskstorage.configuration.Configuration;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.janusgraph.diskstorage.util.StaticArrayBuffer;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;

import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.*;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.DROP_ON_CLEAR;

public class BerkleyJEDataFetcher {
    private String dataPath;
    private String tableName = "edgestore";
    private File directory;
    private Environment environment;
    private Database db;
    private BerkeleyJEStoreManager manager;
    private BerkeleyJEKeyValueStore store;
    private Cursor cur;
    private Transaction tx;

    private static final StaticBuffer.Factory<DatabaseEntry> ENTRY_FACTORY = (array, offset, limit) -> new DatabaseEntry(array,offset,limit-offset);

    public BerkleyJEDataFetcher(String dataFilePath) {
        dataPath = dataFilePath;
        directory = new File(dataPath);
    }

    public BerkleyJEDataFetcher(String dataFilePath, String table) {
        dataPath = dataFilePath;
        tableName = table;
        directory = new File(dataPath);
    }

    public void initializeDataFetchEngine() throws PermanentBackendException {

        initializeEnvironment();
        initializeDatabase();
        initializeStoreManager();
        initializeTransaction();
        initializeStore();
        openCursor();
    }

    public void closeResources() throws PermanentBackendException {
        try {
            cur.close();
//            store.close();
            tx.commit();
            manager.close();
            db.close();
            environment.close();
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to close DB or store instance");
        }
    }

    private void initializeEnvironment() {
        EnvironmentConfig envConfig = new EnvironmentConfig();
        envConfig.setAllowCreate(true);
        envConfig.setTransactional(true);
        environment = new Environment(directory, envConfig);
    }

    private void initializeDatabase() {
        DatabaseConfig dbConfig = new DatabaseConfig();
        dbConfig.setReadOnly(false);
        dbConfig.setAllowCreate(true);
        dbConfig.setTransactional(true);
        db = environment.openDatabase(null, tableName, dbConfig);
    }

    private void initializeStoreManager() throws PermanentBackendException {
        try {
            manager = new BerkeleyJEStoreManager(getGraphConfiguration());
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to open connection to BerkleyDB");
        }

    }

    private Configuration getGraphConfiguration() {
        return BerkeleyDBStorageSetup.getBerkeleyJEConfiguration(dataPath);
    }

    private void initializeTransaction() {
        TransactionConfig txnConfig = new TransactionConfig();
        tx = environment.beginTransaction(null, txnConfig);
    }

    private void initializeStore() {
        store = new BerkeleyJEKeyValueStore(tableName, db, manager);
    }

    private void openCursor() {
        cur = db.openCursor(tx, null);
    }

    public List<KeyValueEntry> getTopKeyValueEntries(Integer top) {
        final List<KeyValueEntry> resultsKeyValueEntry = new ArrayList<>();

        DatabaseEntry foundKey = new DatabaseEntry();
        DatabaseEntry foundData = new DatabaseEntry();

        Integer counter = 1;

        while (cur.getNext(foundKey, foundData, LockMode.DEFAULT) ==
            OperationStatus.SUCCESS) {

            StaticBuffer key = getBuffer(foundKey);
            StaticBuffer value = getBuffer(foundData);

            KeyValueEntry row = new KeyValueEntry(key, value);

            resultsKeyValueEntry.add(row);

            if (counter >= top) {
                break;
            }
            counter += 1;
        }

        return resultsKeyValueEntry;
    }

    public List<KeyValueEntry> getTopKeyValueEntries(Integer top, StaticBuffer key) {
        final List<KeyValueEntry> resultsKeyValueEntry = new ArrayList<>();

        DatabaseEntry findKey = key.as(ENTRY_FACTORY);

        DatabaseEntry foundData = new DatabaseEntry();

        Integer counter = 1;

        OperationStatus status = cur.getSearchKeyRange(findKey, foundData, LockMode.DEFAULT);

        while (status == OperationStatus.SUCCESS) {
            StaticBuffer keySB = getBuffer(findKey);
            StaticBuffer valueSB = getBuffer(foundData);

            KeyValueEntry row = new KeyValueEntry(keySB, valueSB);

            resultsKeyValueEntry.add(row);

            if (counter >= top) {
                break;
            }

            counter += 1;
            status = cur.getNext(findKey, foundData, LockMode.DEFAULT);
        }

        return resultsKeyValueEntry;
    }

    public List<KeyValueEntry> getPrevKeyValueEntries(Integer limit) {
        final List<KeyValueEntry> resultsKeyValueEntry = new ArrayList<>();

        DatabaseEntry foundKey = new DatabaseEntry();
        DatabaseEntry foundData = new DatabaseEntry();

        Integer counter = 1;

        while (cur.getPrev(foundKey, foundData, LockMode.DEFAULT) ==
            OperationStatus.SUCCESS) {

            StaticBuffer key = getBuffer(foundKey);
            StaticBuffer value = getBuffer(foundData);

            KeyValueEntry row = new KeyValueEntry(key, value);

            resultsKeyValueEntry.add(row);

            if (counter > limit) {
                break;
            }
            counter += 1;
        }

        return resultsKeyValueEntry;
    }

    public void moveCursor (StaticBuffer key) {
        DatabaseEntry data = new DatabaseEntry();
        DatabaseEntry keyConverted = key.as(ENTRY_FACTORY);
        cur.getSearchKey(keyConverted, data, LockMode.DEFAULT);
    }

    @Override
    public String toString() {
        return String.format("Data Path: %s Table: %s", dataPath, tableName);
    }

    private static StaticBuffer getBuffer(DatabaseEntry entry) {
        return new StaticArrayBuffer(entry.getData(),entry.getOffset(),entry.getOffset()+entry.getSize());
    }

}
