package com.sstech.janusgraph.common.storage;

import com.sstech.janusgraph.common.utils.GraphUtils;
import com.sstech.janusgraph.common.utils.OSUtils;
import org.janusgraph.StorageSetup;
import org.janusgraph.diskstorage.configuration.ModifiableConfiguration;
import org.janusgraph.diskstorage.configuration.WriteConfiguration;

import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.*;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.CLUSTER_FILE_PATH;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.GRAPH;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.VERSION;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.SCHEMA;


public class SnowFlakeDBStorageSetup extends StorageSetup {
    public static ModifiableConfiguration getSnowFlakeDBConfiguration() {
        return getSnowFlakeDBConfiguration("GRAPHDBTEST");
    }

    private static ModifiableConfiguration getSnowFlakeDBConfiguration(final String graphName) {
        return buildGraphConfiguration()
            .set(STORAGE_BACKEND, "com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager")
            .set(GRAPH, graphName)
            .set(SCHEMA, "GRAPHDBTEST1")
            .set(CLUSTER_FILE_PATH, getClusterFilePath())
            .set(VERSION, "3.10.3");
    }

    public static String getClusterFilePath() {
        return GraphUtils.SnowFlake_Cluster_File;
    }
}
