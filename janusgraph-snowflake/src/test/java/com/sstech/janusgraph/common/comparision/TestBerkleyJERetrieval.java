package com.sstech.janusgraph.common.comparision;

import com.sstech.janusgraph.common.comparision.datafetcher.BerkleyJEDataFetcher;
import com.sstech.janusgraph.common.storage.BerkeleyDBStorageSetup;
import com.sstech.janusgraph.common.utils.GraphUtils;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.StaticBuffer;
import org.janusgraph.diskstorage.keycolumnvalue.keyvalue.KeyValueEntry;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

public class TestBerkleyJERetrieval {
    /*
    This Class takes into consideration that already a BerkleyDB Env(JanusGraph) is defined and exists at the Path specified.
    It then does multiple operation on the DB on specified table name

    1: Asserts the validity of query like number of records fetched is equal to number of records queried
    2: Asserts the validity of data being returned
    3: Asserts the consistency of data being returned across multiple transactions

    NOTE: Makes use of store.insert() and BerkleyDB's Cursor to do operation on DB in SnowFlakeDataFetcher Class.
     */
    private String graphPath;
    private BerkleyJEDataFetcher dataFetcher;

    public TestBerkleyJERetrieval() throws PermanentBackendException {
//        graphPath = "/root/data/graph";
//        graphPath = "D:\\Projects\\Projects\\Freelancing\\Elysium Analytics\\snowflake\\graph";
        graphPath = BerkeleyDBStorageSetup.getDataPath();
//        initializeDataReader();
    }

    @Before
    public void initializeDataReader() throws PermanentBackendException {
        dataFetcher = new BerkleyJEDataFetcher(graphPath);
        System.out.println("DataFetcher class is " + dataFetcher.toString());
        System.out.println(dataFetcher == null);
        dataFetcher.initializeDataFetchEngine();
    }

    @Test
    public void testNumberOfRecordsReceived() {
        Integer recordLimit = 10;
        List<KeyValueEntry> records = dataFetcher.getTopKeyValueEntries(recordLimit);

        Assert.assertEquals(records.size(), recordLimit, 0.001);
        Assert.assertNotNull(records.get(0));
        Assert.assertNotNull(records.get(1));
        Assert.assertNotNull(records.get(2));
    }

    @Test
    public void testTypeOfRecordsReceived() {
        Integer recordLimit = 10;
        List<KeyValueEntry> records = dataFetcher.getTopKeyValueEntries(recordLimit);

        KeyValueEntry firstRecord = records.get(0);
        KeyValueEntry secondRecord = records.get(1);
        KeyValueEntry thirdRecord = records.get(3);

        Assert.assertNotNull(firstRecord.getKey().asByteBuffer());
        Assert.assertNotEquals(firstRecord.getKey(), secondRecord.getKey());
        Assert.assertNotEquals(firstRecord.getKey(), firstRecord.getValue());
        Assert.assertNotNull(thirdRecord.getKey().asByteBuffer());
    }

    @Test
    public void testConsistencyOfRecordsReceived() {
        Integer recordLimit = 5;
        List<KeyValueEntry> records = dataFetcher.getTopKeyValueEntries(recordLimit);

        KeyValueEntry firstRecord = records.get(0);
        StaticBuffer key = firstRecord.getKey();

        List<KeyValueEntry> records2 = dataFetcher.getTopKeyValueEntries(recordLimit, key);
        System.out.println(String.format("records %s records2 %s", records.size(), records2.size()));

        Assert.assertEquals(records.get(0).getKey(), records2.get(0).getKey());
        Assert.assertEquals(records.get(0).getValue(), records2.get(0).getValue());

        Assert.assertEquals(records.get(1).getKey(), records2.get(1).getKey());
        Assert.assertEquals(records.get(1).getValue(), records2.get(1).getValue());

        Assert.assertEquals(records.get(2).getKey(), records2.get(2).getKey());
        Assert.assertEquals(records.get(2).getValue(), records2.get(2).getValue());

        Assert.assertEquals(records.get(3).getKey(), records2.get(3).getKey());
        Assert.assertEquals(records.get(3).getValue(), records2.get(3).getValue());

        Assert.assertEquals(records.get(4).getKey(), records2.get(4).getKey());
        Assert.assertEquals(records.get(4).getValue(), records2.get(4).getValue());
    }

    @After
    public void closeResources() throws Exception {
        if (dataFetcher != null) {
            dataFetcher.closeResources();
        }
    }

}
