package com.sstech.janusgraph.diskstorage;

import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeKCVStoreManager;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.KeyColumnValueStoreTest;
import org.janusgraph.diskstorage.keycolumnvalue.KeyColumnValueStoreManager;

public class SnowFlakeKeyColumnValueStoreTest extends KeyColumnValueStoreTest {
    @Override
    public KeyColumnValueStoreManager openStorageManager() throws BackendException {
        return new SnowFlakeKCVStoreManager();
    }
}
