package com.sstech.janusgraph.snowflake;

import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.*;
import static com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeDBConfigOptions.VERSION;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.STORAGE_BACKEND;
import static org.janusgraph.graphdb.configuration.GraphDatabaseConfiguration.buildGraphConfiguration;
import static org.junit.Assert.*;

import java.time.Instant;

import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageClassSetup;
import com.sstech.janusgraph.common.storage.SnowFlakeDBStorageSetup;
import com.sstech.janusgraph.common.utils.GraphUtils;
import org.apache.commons.configuration.BaseConfiguration;
import org.janusgraph.diskstorage.BackendException;
import org.janusgraph.diskstorage.BaseTransactionConfig;
import org.janusgraph.diskstorage.PermanentBackendException;
import org.janusgraph.diskstorage.configuration.*;
import org.janusgraph.diskstorage.configuration.backend.CommonsConfiguration;
import org.janusgraph.diskstorage.util.time.TimestampProvider;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeStoreManager;
import com.sstech.janusgraph.diskstorage.snowflake.SnowFlakeTransaction;
import com.sstech.janusgraph.snowflake.connection.SnowFlakeConnection;
import com.sstech.janusgraph.snowflake.snowflakedb.*;//SnowFlakeDatabase;

public class TestSnowFlakeDatabase {
    private String tableName = "edgestore";
//    private String graphName = "GRAPHDBTEST";
//    private String clusterFilePath;
//    public static final ConfigNamespace ROOT_NS = new ConfigNamespace(null,"root","Root Configuration Namespace for the JanusGraph Graph Database");

    private Configuration getGraphConfiguration() {
//        clusterFilePath = GraphUtils.SnowFlake_Cluster_File;
        return SnowFlakeDBStorageSetup.getSnowFlakeDBConfiguration();
    }
//
//    public static ModifiableConfiguration buildGraphConfiguration() {
//        return new ModifiableConfiguration(ROOT_NS,
//            new CommonsConfiguration(new BaseConfiguration()),
//            BasicConfiguration.Restriction.NONE);
//    }
    SnowFlakeStoreManager manager;
    SnowFlakeTransaction tx;
    SnowFlakeDatabase sfd;

    /*private void initializeStoreManager() throws PermanentBackendException {
        try {
            manager = new SnowFlakeStoreManager(getGraphConfiguration());
        } catch (BackendException e) {
            throw new PermanentBackendException("Unable to open connection to BerkleyDB");
        }
    }*/

    public TestSnowFlakeDatabase () throws Exception
    {
        //manager = new SnowFlakeStoreManager(null);
//        manager = new SnowFlakeStoreManager(getGraphConfiguration());
//        tx = new SnowFlakeTransaction(GraphUtils.getEmptyTxConfig(), manager);
//        sfd = new SnowFlakeDatabase("janusgraph_ids",tx);

    }

    @Before
    public void initializeInstances() throws Exception {
        SnowFlakeDBStorageClassSetup storageInstanceOpener = new SnowFlakeDBStorageClassSetup(tableName);
        storageInstanceOpener.initializeSFEngine();
        manager = storageInstanceOpener.getManager();
        System.out.println("Initialized manager " + manager);
        tx = storageInstanceOpener.getTransaction();
        System.out.println("Initialized Tx " + tx);
        sfd = storageInstanceOpener.getDB();
        System.out.println("Initialized db " + sfd);
        storageInstanceOpener.closeSFResources();
    }

    @Test
    public void testgetConnection()
    {
        System.out.println("SFD: " + sfd.toString());
        SnowFlakeConnection connection = sfd.getConnection();
        // System.out.println(connection);
        assertNotNull(connection);
        // assertNull(connection);

    }

    @After
    public void closeResources() {

    }

}


