SnowFlake Storage Backend for JanusGraph
Copyright 2019-2020 SSTECH.com. or its affiliates. All Rights Reserved.

This product includes software developed at
SystemSoft Technologied Ltd

**********************
THIRD PARTY COMPONENTS
**********************
This software includes third party software subject to the following copyrights:
- Titan: Distributed Graph Database - Copyright 2012 and onwards Aurelius. Apache License 2.0.
- JanusGraph: Distributed Graph Database - Copyright 2017 and onwards. Apache License 2.0.
- Guava: Google Core Libraries for Java - Copyright 2010 and onwards Google, Inc. Apache License 2.0.
- rexster-service.sh - Copyright (c) 2009-Infinity, TinkerPop [http://tinkerpop.com]. BSD License.

The licenses for these third party components are included in LICENSE.txt.